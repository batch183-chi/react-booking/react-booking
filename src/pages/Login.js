import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";


import { Navigate } from "react-router-dom";
import {Button, Form} from "react-bootstrap";
import Swal from "sweetalert2";

export default function Login(){

	// Allows us to consume the User Context object and it's properties to use for the user validation.
	const {user, setUser} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	// State to determine whether submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	// To enable the submit button:
		// No empty input fields.
	useEffect(() =>{
		if(email !== "" && password !==""){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[email, password])

	// Function to simulate user registration
	function loginUser(e){
		// prevents the page redirection via form submit
		e.preventDefault();

		// Process a fetch request to the corresponding backend API.
			// Syntax:
				// fetch("url"), {options})
					// Converts the information retrieved from the backend into a JavaScript object.
				// .then(res => res.json())
					// Captures the converted data.
				// .then(data => {})
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				// values are coming from our State hooks
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);
			console.log(data.access);

			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			}
			else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}


		})

		// Syntax: localStorage.setItem("propertyName", value);
		// Set the email of the authenticated user in the local storage
		// localStorage.setItem("email", email);

		// setUser({
		// 			// email used upon logging in.
		// 	email: localStorage.getItem("email")
		// })

		// Clear input fields
		setEmail("");
		setPassword("");

		// alert(`${email} has been verified! Welcome back!`);

	}

	// Retrieve user details
	// We will get the payload from the token.
	const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// This will be set to the user state.
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	return(

		(user.id !== null)
		?
			// Redirected to the /courses endpoint.
			<Navigate to="/courses" />
		:

		<>
			<h1 className="my-5 text-center">Login</h1>
			<Form onSubmit = {(e) => loginUser(e)}>
			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
			      </Form.Group>

			      {
			      	isActive
			      	?
				      	<Button variant="primary" type="submit" id="submitBtn">
				      	  Submit
				      	</Button>
			      	:
				      	<Button variant="primary" type="submit" id="submitBtn" disabled>
				      	  Submit
				      	</Button>
			      }
			</Form>
		</>
	)
}