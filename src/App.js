import {useState, useEffect} from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import {UserProvider} from "./UserContext";

import AppNavbar from "./components/AppNavbar";

import Courses from "./pages/Courses";
import CourseView from "./pages/CourseView";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";

import {Container} from "react-bootstrap";
import './App.css';

function App() {

  // Global State - accessible to all components
  // To store the user information and will be used for validating if a user is already logged in on the the app or not.
  const [user, setUser] = useState({
            // current value is null
    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  })

  // We can check the changes in our User state.
  console.log(user);

  // Function for clearing localStorage on logout
  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(() =>{
    console.log(user);
    console.log(localStorage);
  }, [user])

  // To update the User State upon page load if a user already exist.
  useEffect(() =>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      // set the user states values with the user details upon successful login
      if(typeof data._id !== "undefined"){
          setUser({
            //undefined
            id: data._id,
            isAdmin: data.isAdmin
          })
      }
      else{
          setUser({
              //undefined
              id: null,
              isAdmin: null
            })
      }
    })
  }, [])

  return (
    // ReactJS does not like rendering two adjacent elements. Instead, the adjacent element must be wrapped by a parent element using react fragment <></>.

    // ReactJS is a single page application (SPA)

    // We store information in the context by providing the information using the corresponding "Provider" component and passing the information via the "value" prop/attribute.
    <UserProvider value={{user, setUser, unsetUser}}>
      {/*Router component is used to wrap around all components which will have access to our routing system.*/}
      <Router>
        <AppNavbar />
        <Container fluid>
            {/*Routes hold all our Route Components.*/}
            <Routes>
            {/*
                -Route assigns an endpoint and displays the appropriate page component for that endpoint
                -"path" attribute assigns the endpoint.
                -"element" attribute assigns page component to be displayed at the endpoint.
            */}
                <Route exact path ="/" element={<Home />} />
                <Route exact path ="/courses" element={<Courses />} />
                <Route exact path ="/courses/:courseId" element={<CourseView />} />
                <Route exact path ="/register" element={<Register />} />
                <Route exact path ="/login" element={<Login />} />
                <Route exact path ="/logout" element={<Logout />} />
                <Route exact path ="*" element={<Error />} />
            </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
